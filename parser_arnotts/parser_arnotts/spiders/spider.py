# coding: utf8
import scrapy
import re
import json
import csv
import sys
from parser_arnotts.items import ParserArnottsItem

class ParserArnottsSpider(scrapy.Spider):

    name = "arnotts"

    def start_requests(self):
        url = 'http://www.arnotts.ie/'
        yield scrapy.Request(url=url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.xpath(".//ul[@class='menu-category-column']/li/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_item_list)


    def parse_item_list(self, response):
        itemCountSource =  response.xpath(".//span[@class='pag-items-show']/text()").extract_first()
        reItemCount = re.search(r'(\d+)',itemCountSource)
        itemCount = reItemCount.group(1)
        for itm in range(0,int(itemCount),12):
            url = response.url + '?sz=12&productsearch=true&start='+ str(itm)+'&format=page-element&viewall=true'
            yield scrapy.Request(url=url, callback=self.parse_item_chk)

    def parse_item_chk(self, response):
        urls = response.xpath(".//a[@class='product-description-link js-product-url']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        product = ParserArnottsItem()
        product['url'] = response.url
        reURL = re.search(r'\/(\w+)\.html', response.url)
        itemId = reURL.group(1)
        product['id'] = itemId
        product['produckt_group'] = response.xpath(".//div[@class='breadcrumb-element-wrapper'][position()=3]//span/text()").extract_first(default='')
        product['img'] = response.xpath(".//img[@class='primary-image']/@src").extract_first(default='')
        product['name'] = response.xpath(".//span[@class='product-name-title']/text()").extract_first(default='').replace('"','&#34;').replace(',','&#44;').strip()
        price = response.xpath(".//meta[@itemprop='price']/@content").extract_first()
        try:
            product['price'] = float(price.replace('"','&#34;').replace(',','&#44;').replace(u'€',''))
        except:
            product['price'] = 0
        yield product


















